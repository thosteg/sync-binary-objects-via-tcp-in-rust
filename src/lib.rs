#[allow(unused_imports)]
use tokio::net::TcpListener;
#[allow(unused_imports)]
use tokio::net::TcpStream;
#[allow(unused_imports)]
use tokio::io::AsyncWriteExt;


// use tokio::prelude::*;

// Idea:
// - listen on a socket
// - connect ourself to that
// - send message and receive it
// - done
// 
// Do the same via some sort of queue
// Do the same via some sort of message passing
//
#[tokio::test]
async fn test_tcp_to_self() {
    const ADDR : &str= "127.0.0.1:12345";
    const NUM_BYTES : usize = 5;
    const BUF_READ_SIZE : usize = 1000;

    let reader = tokio::spawn(async {
        // This task connects and receives
        // This spawned task must return something proper
        match TcpStream::connect(ADDR).await {
            Ok(stream) => {
                let mut buf = [0; BUF_READ_SIZE];
                let n = stream.peek(&mut buf).await.unwrap();
                Ok(n)
            }
            Err(_) => {
                Err(())
            }
        }
    });

    {
        // Main task listens and sends once
        let listener = TcpListener::bind(ADDR).await.unwrap();
        let (mut socket, _) = listener.accept().await.unwrap();

        let mut buf : [u8; NUM_BYTES] = [0; NUM_BYTES];
        let num_written = socket.write(&mut buf).await.unwrap();

        assert_eq!(num_written, NUM_BYTES);
    }

    let res_num_read = reader.await.unwrap();
    assert_eq!(res_num_read, Ok(NUM_BYTES));
}

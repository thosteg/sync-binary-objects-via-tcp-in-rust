use std::collections::HashMap;

#[derive(Debug, PartialEq, Clone)]
struct A {
    v: i32,
}

#[derive(Debug, PartialEq, Clone)]
struct B {
    v: f64,
}

#[allow(dead_code)]
#[derive(Debug, PartialEq, Clone)]
enum Values {
    Ta(A),
    Tb(B),
}

type ValuesMap = HashMap<i32, Values>;

#[test]
fn test_modify_value_in_a() {
    let mut p: HashMap<i32, Values> = HashMap::new();

    p.insert(1, Values::Ta(A { v: 7 }));
    p.insert(2, Values::Tb(B { v: 32.6 }));

    let expected = Values::Ta(A { v: 7 });
    let actual = p.get(&1).unwrap();
    assert_eq!(actual, &expected);
}

#[allow(dead_code)]
fn sync_once<'x>(a: &'x ValuesMap, b: &mut ValuesMap, key: &i32) -> Result<Option<&'x Values>, ()> {
    // If both sides contain the given key, and it is not equal, sync it and return Some(Value).
    // If both sides contain the given key, and it is equal, skip sync and return None.
    // If any side does not contain the given key, return Err.
    match a.get(&key) {
        None => Err(()),
        Some(source) => match b.get(&key) {
            None => Err(()),
            Some(target) => {
                if source == target {
                    Ok(None)
                } else {
                    b.entry(*key).and_modify(|e| *e = source.clone());
                    Ok(Some(source))
                }
            }
        },
    }
}

#[test]
fn test_single_initial_sync() {
    // Construct: a, b, sync
    // Setup:
    // - k_1, v_1 in a
    // - k_1, v_2 in b
    // - test.subscribe(b, k_1)
    // - sync.add_synced_key(k_1, SYNC_INITIAL)
    // - test.await() == Some(k_1 -> v_1)

    const K: i32 = 123;
    const V_A: i32 = 7;
    const V_B: i32 = 6;
    const VAL_A: Values = Values::Ta(A { v: V_A });
    const VAL_B: Values = Values::Ta(A { v: V_B });

    let mut a: HashMap<i32, Values> = HashMap::new();
    a.insert(K, VAL_A);

    let mut b: HashMap<i32, Values> = HashMap::new();
    b.insert(K, VAL_B);

    // Initially values are not equal
    assert_ne!(a.get(&K).unwrap(), b.get(&K).unwrap());

    // Sync value once leads to sync
    assert_eq!(
        sync_once(&a, &mut b, &K),
        Ok(Some(&Values::Ta(A { v: V_A })))
    );

    // Second sync leads does not need to do anything
    assert_eq!(sync_once(&a, &mut b, &K), Ok(None));

    // Sync fails if key nonexistent in either hashmap
    a.remove(&K);
    assert_eq!(sync_once(&a, &mut b, &K), Err(()));

    a.insert(K, VAL_A);
    b.remove(&K);
    assert_eq!(sync_once(&a, &mut b, &K), Err(()));
}


async fn sleeper() {
}

#[tokio::test]
async fn tokio_test() {
    sleeper().await;
}


async fn say_world() {
    println!("world");
}

// A)
//   - dictionary A  (int -> struct containing double)
//   - dictionary B
//   - task transfers single value
//   - test set value in A, call transfer function, test get value in B, equality

#[tokio::main]
async fn main() {
    let op = say_world();
    println!("hello");
    op.await;
}

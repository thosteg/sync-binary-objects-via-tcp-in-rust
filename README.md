# sync-binary-objects-via-tcp-in-rust

Exercise: 

This shall be my personal try at creating a small "synch-thing" between two object stores. Changes to objects on selected objects on one side will be transfered to matching objects on the second side (and vice versa).

Initial step is internal communication with TCP as a second step.

The main intention is to gain the necessary experience myself.

## Description

Participants:

* Two data stores
* synch tool as a proxy

Data store:

* Each data store is a cross breed between a dictionary (a la redis) with subscriptions (a la mqtt) and (as a later implementation step) preserved recent history
* That is:
** Dictionary: item data stored by id
** Recent history of data (optional) limited to a maximum number of changes
* Item id: i32 (for now)
* Item data: struct
* All allowed data types (structs) are known beforehand, and encapsulated as enums

Synch-Tool:

* Is given a list of ids that are to be synched a -> b and vice versa
* The lists a -> b and b -> a may differ
* Optional: Filters based on the content of the updated data
* Optional: Execution of individual or all updates based on trigger or (as described above) on item change
* Optional: Multiple differing lists with different filters or execution triggers 

Communication:

* Both stores can communicate with each other
* Additional communication to each store by separate participants is also possible (add, modify, delete item)
* Communication is done by messages:
** GET current value -> VALUE (possibly including id)
** POST value
** SUBSCRIBE to item
** UNSUBSCRIBE from item
* REQUEST IDENTIFIER: if a command is prefixed with this, it will be mirrored to the caller before each response. A command may have a specific confirmation of some sort.

## Usage

No implementation as so far.

## Roadmap

1. Simplest implementation in one process: two dictionaries and one coupler
2. Test for checking that change to an item in (a) results in an item in (b)

## Authors and acknowledgment

Initial commit - documentation by the author only.

## License

Feel free to use snippets for whatever purpose. License may be updated later.

